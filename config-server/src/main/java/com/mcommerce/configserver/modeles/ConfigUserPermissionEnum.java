package com.mcommerce.configserver.modeles;

public enum ConfigUserPermissionEnum {
    SERVER_READ("server:read"),
    ADMIN_READ("admin:read");

    private final String permission;

    ConfigUserPermissionEnum(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
