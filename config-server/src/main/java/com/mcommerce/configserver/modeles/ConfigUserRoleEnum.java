package com.mcommerce.configserver.modeles;

import com.google.common.collect.Sets;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

import static com.mcommerce.configserver.modeles.ConfigUserPermissionEnum.*;


public enum ConfigUserRoleEnum {
    SERVER_SERVICE(Sets.newHashSet(SERVER_READ)),
    ADMIN(Sets.newHashSet(SERVER_READ, ADMIN_READ));

    private final Set<ConfigUserPermissionEnum> permissions;

    ConfigUserRoleEnum(Set<ConfigUserPermissionEnum> permissions) {
        this.permissions = permissions;
    }

    public Set<ConfigUserPermissionEnum> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getGrantedAuthorities() {
        Set<SimpleGrantedAuthority> permissions = getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }
}
