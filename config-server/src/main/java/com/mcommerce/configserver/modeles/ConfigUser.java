package com.mcommerce.configserver.modeles;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

public class ConfigUser implements UserDetails {

    private final Set<? extends GrantedAuthority> grantedAuthorities;
    private final String password;
    private final String username;
    private final boolean accounteNonExpired;
    private final boolean accounteNonLocked;
    private final boolean credentialsExpired;
    private final boolean enabled;

    public ConfigUser(Set<? extends GrantedAuthority> grantedAuthorities,
                      String password,
                      String username,
                      boolean accounteNonExpired,
                      boolean accounteNonLocked,
                      boolean credentialsExpired,
                      boolean enabled) {
        this.grantedAuthorities = grantedAuthorities;
        this.password = password;
        this.username = username;
        this.accounteNonExpired = accounteNonExpired;
        this.accounteNonLocked = accounteNonLocked;
        this.credentialsExpired = credentialsExpired;
        this.enabled = enabled;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accounteNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accounteNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
