package com.mcommerce.configserver.dao;

import com.google.common.collect.Lists;
import com.mcommerce.configserver.modeles.ConfigUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.mcommerce.configserver.modeles.ConfigUserRoleEnum.*;


@Repository("fake")
public class FakeApplicationUserDaoImpl implements ConfigUserDao {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public FakeApplicationUserDaoImpl(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<ConfigUser> selectApplicationUserByUsername(String username) {
        return getConfigUsers()
                .stream()
                .filter(applicationUser -> username.equals(applicationUser.getUsername()))
                .findFirst();

    }

    @Override
    public Optional<List<ConfigUser>> getAllUsers() {
        List<ConfigUser> applicationUsers = getConfigUsers();
        return Optional.ofNullable(applicationUsers);
    }

    private List<ConfigUser> getConfigUsers() {

        List<ConfigUser> configUsers = Lists.newArrayList(

                new ConfigUser(
                        SERVER_SERVICE.getGrantedAuthorities(),
                        passwordEncoder.encode("password"),
                        "produitService",
                        true,
                        true,
                        true,
                        true
                ),
                new ConfigUser(
                        SERVER_SERVICE.getGrantedAuthorities(),
                        passwordEncoder.encode("password"),
                        "gatewayService",
                        true,
                        true,
                        true,
                        true
                ),
                new ConfigUser(
                        ADMIN.getGrantedAuthorities(),
                        passwordEncoder.encode("password"),
                        "admin",
                        true,
                        true,
                        true,
                        true
                )
        );

        return configUsers;
    }
}
