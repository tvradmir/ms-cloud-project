package com.mcommerce.configserver.dao;

import com.mcommerce.configserver.modeles.ConfigUser;

import java.util.List;
import java.util.Optional;

public interface ConfigUserDao {

    Optional<ConfigUser> selectApplicationUserByUsername(String username);

    Optional<List<ConfigUser>> getAllUsers();

}
