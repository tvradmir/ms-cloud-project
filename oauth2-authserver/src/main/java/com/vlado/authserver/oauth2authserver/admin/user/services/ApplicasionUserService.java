package com.vlado.authserver.oauth2authserver.admin.user.services;

import com.vlado.authserver.oauth2authserver.admin.user.modeles.ApplicationUser;

import java.util.List;
import java.util.Optional;

public interface ApplicasionUserService {

    Optional<List<ApplicationUser>> getAllApplicationUsers();

    Optional<ApplicationUser> selectApplicationUserByUsername(String username);

}
