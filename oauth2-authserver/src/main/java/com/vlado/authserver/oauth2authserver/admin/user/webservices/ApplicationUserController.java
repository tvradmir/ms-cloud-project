package com.vlado.authserver.oauth2authserver.admin.user.webservices;


import com.vlado.authserver.oauth2authserver.admin.user.modeles.ApplicationUser;
import com.vlado.authserver.oauth2authserver.admin.user.services.ApplicasionUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/management/users")
public class ApplicationUserController {

    @Autowired
    private ApplicasionUserService applicasionUserService;

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_ADMINTRAINEE')")
    Optional<List<ApplicationUser>> getAllUsers(){
        return  applicasionUserService.getAllApplicationUsers();
    }

    @GetMapping("/currentUser")
    Optional<ApplicationUser> getCurrentUser(){
        Authentication authentication = getAuthentication();
        Optional.ofNullable(authentication.getName());
        return applicasionUserService.selectApplicationUserByUsername(authentication.getName());
    }

    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @RequestMapping({ "/user", "/me" })
    public Map<String, String> user(Principal principal) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("name", principal.getName());
        return map;
    }

    @GetMapping("/current")
    public Principal getUser(Principal principal) {
        return principal;
    }


}
