package com.vlado.authserver.oauth2authserver.admin.user.services;

import com.vlado.authserver.oauth2authserver.admin.user.dao.ApplicationUserDao;
import com.vlado.authserver.oauth2authserver.admin.user.modeles.ApplicationUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ApplicationUserServiceImpl implements ApplicasionUserService {

    @Autowired
    private ApplicationUserDao applicasionUserdao;


    @Override
    public Optional<List<ApplicationUser>> getAllApplicationUsers() {
        return applicasionUserdao.getAllUsers();
    }


    @Override
    public Optional<ApplicationUser> selectApplicationUserByUsername(String username) {
        return applicasionUserdao.selectApplicationUserByUsername(username);
    }
}
