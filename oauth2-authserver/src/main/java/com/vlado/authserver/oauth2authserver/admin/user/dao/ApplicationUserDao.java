package com.vlado.authserver.oauth2authserver.admin.user.dao;

import com.vlado.authserver.oauth2authserver.admin.user.modeles.ApplicationUser;

import java.util.List;
import java.util.Optional;

public interface ApplicationUserDao {

    Optional<ApplicationUser> selectApplicationUserByUsername(String username);

    Optional<List<ApplicationUser>> getAllUsers();

}
